﻿#include <iostream>

using namespace std;

#define N 10

void task(int n, bool even)
{
    if (even)
    {
        for (int i = 0; i <= n; i++)
        {
            if (i % 2 == 0)
            {
                cout << i << endl;
            }
        }
    }
    else
    {
        for (int i = 0; i <= n; i++)
        {
            if (i % 2 != 0)
            {
                cout << i << endl;
            }
        }
    }
}

int main()
{
    for (int i = 0; i <= N; i++)
    {
        if (i % 2 == 0)
        {
            cout << i << endl;
        }
    }

    bool even = false;

    task(N, even);

    return 0;
}